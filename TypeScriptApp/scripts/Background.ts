// Interface
interface IPoint {
    getDist(): number;
}

// Module
module TypeScriptApp {
    // Class
    export class Background {
        tile: HTMLImageElement;

        constructor(path: string) {
            this.tile = new Image();
            this.tile.src = path;
        }

        draw(ctx: CanvasRenderingContext2D) {
            for (var i = 0; i < ctx.canvas.width; i += this.tile.width) {
                for (var j = 0; j < ctx.canvas.width; j += this.tile.height) {
                    ctx.drawImage(this.tile, i, j);
                }
            }
        }
    }
}