// Module
module TypeScriptApp {
    // Class
    export class Game {
        tick1: number;
        tick2: number;
        tick3: number;
        ctx: CanvasRenderingContext2D;
        bg: Background;
        units: Array<unit>;
        zergList: Array<animatedTile>;
        count: number;
        timeStarted: number;
        oldMouseX: number;
        oldMouseY: number;

        constructor(ctx: CanvasRenderingContext2D) {
            this.bg = new Background("../res/grass.jpg");
            this.timeStarted = new Date().getTime();
            this.count = 0;
            this.units = new Array<unit>();
            this.zergList = new Array<animatedTile>();
            //for (var i = 0; i < 10; i++) {
                //zerg_down_right.png
            this.units.push(new unit("../res/MechMove.png", 100, 100, 90, 2));                
            this.units.push(new unit("../res/MechMove.png", 100, 300, 90, 2));
            this.units.push(new unit("../res/MechMove.png", 100, 500, 90, 2));
                //this.zergList.push(new animatedTile("zerg_down_right.png", Math.random() * 800, Math.random() * 600, 9));
            //}
            this.ctx = ctx;
            this.oldMouseX = 0;
            this.oldMouseY = 0;
        }

        Run() {
            this.tick1 = setInterval(() => this.DrawLoop(), 25);
            this.tick2 = setInterval(() => this.LogicLoop(), 75);
            this.tick3 = setInterval(() => this.FpsLoop(), 3000);
        }

        DrawLoop() {
            var ctx = this.ctx;
            this.bg.draw(ctx);
            this.units.forEach(function (unit1) {
                unit1.draw(ctx);
            });
            //this.zergList.forEach(function (zerg) {
            //    zerg.draw(ctx);
            //});
            this.count++;
            document.getElementById("frame").innerText = 'frame: ' + this.count;
            document.getElementById("count").innerText = this.units.length.toString() + ' entities';
        }

        LogicLoop() {
            var ctx = this.ctx;
            //this.zergList.forEach(function (zerg) {
            //    zerg.update();
            //});            
            this.units.forEach(function (unit1) {
                if (unit1.x > ctx.canvas.width || unit1.y > ctx.canvas.height || unit1.x < 0 || unit1.y < 0)
                    unit1.facing += 180;
                unit1.Update();
                //unit1.Move(3);
            });
        }

        FpsLoop() {
            var currTime = new Date().getTime();
            document.getElementById("time").innerText = (this.count / (currTime - this.timeStarted) * 1000).toString() + ' fps';
            this.count = 0;
            this.timeStarted = new Date().getTime();
        }

        onMouseDown(event: MouseEvent) {
            //var ctx = this.ctx;
            //this.units.forEach(function (unit1, i) {
            //    if (unit1.contains(event.x, event.y))
            //        unit1.dragged = true;
            //    else {
            //        //unit1.selected = false;
            //        unit1.dragged = false;
            //    }
            //});
        }

        onMouseUp(event: MouseEvent) {
            this.units.forEach(function (unit1, i) {
                //unit1.dragged = false;
                if (unit1.selected) {
                    unit1.gtX = event.x;
                    unit1.gtY = event.y;
                }
                if (unit1.contains(event.x, event.y))
                    unit1.selected = true;
                else //{
                    unit1.selected = false;
                    //unit1.dragged = false;
                //}
            });
        }

        onMouseMove(event: MouseEvent) {
            //this.units.forEach(function (unit1, i) {
            //    //if (unit1.dragged) {
            //    //    unit1.x = event.x - unit1.frameWidth / 2;
            //    //    unit1.y = event.y - unit1.frameHeight / 2;
            //    //}
            //    //if (unit1.selected) {
            //    //    if (this.oldMouseY < event.y)
            //    //        unit1.facing += 3;
            //    //    else
            //    //        unit1.facing -= 3;
            //    //    this.oldMouseY = event.y;
            //    //}
            //});
        }

        AddEntity() {
            for (var i = 0; i < 10; i++) {
                this.units.push(new unit("../res/MechMove.png", Math.random() * 800, Math.random() * 600, Math.random() * 360, 2));
            }
        }
    }
}