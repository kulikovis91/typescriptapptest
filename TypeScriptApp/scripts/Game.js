// Module
var TypeScriptApp;
(function (TypeScriptApp) {
    // Class
    var Game = (function () {
        function Game(ctx) {
            this.bg = new TypeScriptApp.Background("../res/grass.jpg");
            this.timeStarted = new Date().getTime();
            this.count = 0;
            this.units = new Array();
            this.zergList = new Array();

            //for (var i = 0; i < 10; i++) {
            //zerg_down_right.png
            this.units.push(new TypeScriptApp.unit("../res/MechMove.png", 100, 100, 90, 2));
            this.units.push(new TypeScriptApp.unit("../res/MechMove.png", 100, 300, 90, 2));
            this.units.push(new TypeScriptApp.unit("../res/MechMove.png", 100, 500, 90, 2));

            //this.zergList.push(new animatedTile("zerg_down_right.png", Math.random() * 800, Math.random() * 600, 9));
            //}
            this.ctx = ctx;
            this.oldMouseX = 0;
            this.oldMouseY = 0;
        }
        Game.prototype.Run = function () {
            var _this = this;
            this.tick1 = setInterval(function () {
                return _this.DrawLoop();
            }, 25);
            this.tick2 = setInterval(function () {
                return _this.LogicLoop();
            }, 75);
            this.tick3 = setInterval(function () {
                return _this.FpsLoop();
            }, 3000);
        };

        Game.prototype.DrawLoop = function () {
            var ctx = this.ctx;
            this.bg.draw(ctx);
            this.units.forEach(function (unit1) {
                unit1.draw(ctx);
            });

            //this.zergList.forEach(function (zerg) {
            //    zerg.draw(ctx);
            //});
            this.count++;
            document.getElementById("frame").innerText = 'frame: ' + this.count;
            document.getElementById("count").innerText = this.units.length.toString() + ' entities';
        };

        Game.prototype.LogicLoop = function () {
            var ctx = this.ctx;

            //this.zergList.forEach(function (zerg) {
            //    zerg.update();
            //});
            this.units.forEach(function (unit1) {
                if (unit1.x > ctx.canvas.width || unit1.y > ctx.canvas.height || unit1.x < 0 || unit1.y < 0)
                    unit1.facing += 180;
                unit1.Update();
                //unit1.Move(3);
            });
        };

        Game.prototype.FpsLoop = function () {
            var currTime = new Date().getTime();
            document.getElementById("time").innerText = (this.count / (currTime - this.timeStarted) * 1000).toString() + ' fps';
            this.count = 0;
            this.timeStarted = new Date().getTime();
        };

        Game.prototype.onMouseDown = function (event) {
            //var ctx = this.ctx;
            //this.units.forEach(function (unit1, i) {
            //    if (unit1.contains(event.x, event.y))
            //        unit1.dragged = true;
            //    else {
            //        //unit1.selected = false;
            //        unit1.dragged = false;
            //    }
            //});
        };

        Game.prototype.onMouseUp = function (event) {
            this.units.forEach(function (unit1, i) {
                if (unit1.selected) {
                    unit1.gtX = event.x;
                    unit1.gtY = event.y;
                }
                if (unit1.contains(event.x, event.y))
                    unit1.selected = true;
else
                    unit1.selected = false;
                //unit1.dragged = false;
                //}
            });
        };

        Game.prototype.onMouseMove = function (event) {
            //this.units.forEach(function (unit1, i) {
            //    //if (unit1.dragged) {
            //    //    unit1.x = event.x - unit1.frameWidth / 2;
            //    //    unit1.y = event.y - unit1.frameHeight / 2;
            //    //}
            //    //if (unit1.selected) {
            //    //    if (this.oldMouseY < event.y)
            //    //        unit1.facing += 3;
            //    //    else
            //    //        unit1.facing -= 3;
            //    //    this.oldMouseY = event.y;
            //    //}
            //});
        };

        Game.prototype.AddEntity = function () {
            for (var i = 0; i < 10; i++) {
                this.units.push(new TypeScriptApp.unit("../res/MechMove.png", Math.random() * 800, Math.random() * 600, Math.random() * 360, 2));
            }
        };
        return Game;
    })();
    TypeScriptApp.Game = Game;
})(TypeScriptApp || (TypeScriptApp = {}));
//# sourceMappingURL=Game.js.map
