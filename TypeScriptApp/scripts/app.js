///<reference path='Units.ts'/>
///<reference path='Game.ts'/>
///<reference path='Background.ts'/>
var TypeScriptApp;
(function (TypeScriptApp) {
    window.onload = function () {
        var cv = document.getElementById("canv");
        var ctx = cv.getContext("2d");
        cv.height = 768;
        cv.width = 1024;
        var GameInstance = new TypeScriptApp.Game(ctx);
        GameInstance.Run();

        document.getElementById("canv").onmousedown = function (event) {
            GameInstance.onMouseDown(event);
        };

        document.getElementById("canv").onmouseup = function (event) {
            GameInstance.onMouseUp(event);
        };

        document.getElementById("canv").onmousemove = function (event) {
            GameInstance.onMouseMove(event);
        };

        document.getElementById("more").onclick = function () {
            GameInstance.AddEntity();
        };
    };
})(TypeScriptApp || (TypeScriptApp = {}));
;
//# sourceMappingURL=app.js.map
