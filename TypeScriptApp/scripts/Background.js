// Module
var TypeScriptApp;
(function (TypeScriptApp) {
    // Class
    var Background = (function () {
        function Background(path) {
            this.tile = new Image();
            this.tile.src = path;
        }
        Background.prototype.draw = function (ctx) {
            for (var i = 0; i < ctx.canvas.width; i += this.tile.width) {
                for (var j = 0; j < ctx.canvas.width; j += this.tile.height) {
                    ctx.drawImage(this.tile, i, j);
                }
            }
        };
        return Background;
    })();
    TypeScriptApp.Background = Background;
})(TypeScriptApp || (TypeScriptApp = {}));
//# sourceMappingURL=Background.js.map
