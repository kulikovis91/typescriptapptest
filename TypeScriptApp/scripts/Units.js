// Module
var TypeScriptApp;
(function (TypeScriptApp) {
    // Class
    var unit = (function () {
        function unit(path, x, y, facing, framesTotal) {
            this.tile = new Image();
            this.tile.src = path;
            this.x = x;
            this.y = y;
            this.facing = facing;
            this.paused = true;
            this.frame = 0;
            this.framesTotal = framesTotal;
            this.frameHeight = this.tile.height;
            this.frameWidth = 110;
        }
        unit.prototype.contains = function (x, y) {
            if (x > this.x && y > this.y && y < this.y + this.frameHeight && x < this.x + this.frameWidth)
                return true;
else
                return false;
        };

        unit.prototype.draw = function (ctx) {
            //ctx.drawImage(this.tile, this.x, this.y);
            ctx.drawImage(this.tile, this.frame * this.frameWidth, 0, this.frameWidth, this.frameWidth, this.x, this.y, 110, 110);
            if (this.selected) {
                ctx.strokeStyle = "#FF0000";
                ctx.strokeRect(this.x, this.y, this.frameWidth, this.frameHeight);
                //ctx.beginPath();
                //ctx.moveTo(this.x + this.tile.width / 2, this.y + this.tile.height / 2);
                //ctx.lineTo(this.gtX, this.gtY);
                //ctx.stroke();
                //ctx.closePath();
            }
            if (this.dragged) {
                ctx.strokeStyle = "#FFFF00";
                ctx.strokeRect(this.x, this.y, this.frameWidth, this.frameHeight);
            }
            //ctx.strokeStyle = "#FF0000";
            //ctx.beginPath();
            //ctx.moveTo(this.x + this.tile.width / 2, this.y + this.tile.height / 2);
            //ctx.lineTo(this.markX + this.x + this.tile.width / 2, this.markY + this.y + this.tile.height / 2);
            //ctx.stroke();
            //ctx.closePath();
            //ctx.fillStyle = "#FFFFFF";
            //ctx.fillText(this.facing.toString(), this.x, this.y - 14);
        };

        unit.prototype.Update = function () {
            if (!this.paused)
                this.frame++;
            if (this.frame > this.framesTotal)
                this.frame = 0;
            if (this.gtX > 0 && this.gtY > 0) {
                this.play();
                if (Math.abs(this.x + this.frameWidth / 2 - this.gtX) > 1 && Math.abs(this.y + this.frameHeight - this.gtY) > 1) {
                    this.GetDirection(this.gtX, this.gtY);
                    this.Move(3);
                } else {
                    this.pause();
                    this.gtX = 0;
                    this.gtY = 0;
                }
            }
        };

        unit.prototype.Move = function (speed) {
            var dx = speed * Math.cos((this.facing - 90) * Math.PI / 180);
            var dy = speed * Math.sin((this.facing - 90) * Math.PI / 180);
            this.x += dx;
            this.y += dy;
        };

        unit.prototype.GetDirection = function (gtX, gtY) {
            var centerX = this.x + this.frameWidth / 2;
            var centerY = this.y + this.frameHeight / 2;
            var dX = centerX - gtX;
            var dY = centerY - gtY;
            var tmpFacing = Math.acos(dX / Math.sqrt(dX * dX + dY * dY)) * 180 / Math.PI;

            if (dX > 0 && dY < 0) {
                this.facing = -tmpFacing + 270;
            } else if (dY > 0 && dX < 0) {
                this.facing = tmpFacing - 90;
            } else if (dX < 0) {
                this.facing = 270 - tmpFacing;
            } else if (dY > 0) {
                this.facing = 270 + tmpFacing;
            }
        };

        unit.prototype.update = function () {
            if (!this.paused)
                this.frame++;
            if (this.frame > this.framesTotal)
                this.frame = 0;
        };

        unit.prototype.pause = function () {
            this.paused = true;
        };

        unit.prototype.play = function () {
            this.paused = false;
        };
        return unit;
    })();
    TypeScriptApp.unit = unit;

    var animatedTile = (function () {
        function animatedTile(path, x, y, framesTotal) {
            this.tile = new Image();
            this.tile.src = path;
            this.x = x;
            this.y = y;
            this.paused = false;
            this.frame = 0;
            this.framesTotal = framesTotal;
            this.frameHeight = this.tile.height;
            this.frameWidth = 64;
        }
        animatedTile.prototype.draw = function (ctx) {
            ctx.drawImage(this.tile, this.frame * this.frameWidth, 0, this.frameWidth, this.frameWidth, this.x, this.y, 64, 64);
        };

        animatedTile.prototype.update = function () {
            if (!this.paused)
                this.frame++;
            if (this.frame > this.framesTotal)
                this.frame = 0;
        };

        animatedTile.prototype.pause = function () {
            this.paused = false;
        };

        animatedTile.prototype.play = function () {
            this.paused = false;
        };

        animatedTile.prototype.GetDirection = function (gtX, gtY) {
            var centerX = this.x + this.frameWidth / 2;
            var centerY = this.y + this.frameHeight / 2;
            var dX = centerX - gtX;
            var dY = centerY - gtY;
            var tmpFacing = Math.acos(dX / Math.sqrt(dX * dX + dY * dY)) * 180 / Math.PI;

            if (dX > 0 && dY < 0) {
                this.facing = -tmpFacing + 270;
            } else if (dY > 0 && dX < 0) {
                this.facing = tmpFacing - 90;
            } else if (dX < 0) {
                this.facing = 270 - tmpFacing;
            } else if (dY > 0) {
                this.facing = 270 + tmpFacing;
            }
        };
        return animatedTile;
    })();
    TypeScriptApp.animatedTile = animatedTile;
})(TypeScriptApp || (TypeScriptApp = {}));
//# sourceMappingURL=Units.js.map
