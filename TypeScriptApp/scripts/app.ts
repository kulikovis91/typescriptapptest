///<reference path='Units.ts'/>
///<reference path='Game.ts'/>
///<reference path='Background.ts'/>
module TypeScriptApp {
    window.onload = () => {
        var cv = <HTMLCanvasElement>document.getElementById("canv");
        var ctx = cv.getContext("2d");
        cv.height = 768;
        cv.width = 1024;
        var GameInstance = new Game(ctx);
        GameInstance.Run();

        document.getElementById("canv").onmousedown = (event: MouseEvent) => {
            GameInstance.onMouseDown(event);
        };

        document.getElementById("canv").onmouseup = (event: MouseEvent) => {
            GameInstance.onMouseUp(event);
        };

        document.getElementById("canv").onmousemove = (event: MouseEvent) => {
            GameInstance.onMouseMove(event);
        };

        document.getElementById("more").onclick = () => {
            GameInstance.AddEntity();
        };
    }
};